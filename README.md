# covidmis.info

A website that visualizes COVID-19 related misinformation. It aggregates and groups fact checks and shows their spread within news articles on the Web.

The project can be accessed at this URL: https://covidmis.info

The website is no longer being updated.

## Background

Misinformation during the COVID-19 pandemic poses a serious threat to public health as well as spreads stigma and fear. Recognizing this threat, numerous fact checking sites continuously monitor misinformation related to the disease and publish information fact-checked by experts. Exploring this knowledge base of fact checks provides potential for better understanding how misinformation regarding COVID-19 evolves and relates to other content. CovidMis.info is a website that aims to support the exploration of fact checks and related Web articles. It builds on Monant, a platform for crawling and processing misinformation-related data from the Web. Based on the data, it provides visualizations of relations between fact checks and fact checks and Web articles.

Read more about this project here: https://matus.tomlein.org/blogs/covidmisinfo
